#include "StdAfx.h"
#include "Vector3.h"

Vector3 Vector3::UnitX(1, 0, 0);
Vector3 Vector3::UnitY(0, 1, 0);
Vector3 Vector3::UnitZ(0, 0, 1);