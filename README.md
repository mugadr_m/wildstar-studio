# README #

Wildstar Studio is a tool that lets you explore the game assets of wildstar.

## What is this repository for? ##

* Learn the formats used in the game using the source code
* Explore files and content of the game

## How do I get set up? ##

### Binary build ###

1. Download [Launcher.exe](https://bitbucket.org/mugadr_m/wildstar-studio/downloads/Launcher.exe)
2. Run Launcher.exe and wait for it to complete the downloads
3. Run WildstarStudio.exe either from the launcher or directly from the binary

### Build from source ###

** Prerequisites **

* Visual Studio 2013

* Windows Vista or newer

* DirectX SDK installed

** Build **

1. Checkout the source code

2. Open WildstarStudio.sln

3. Choose build configuration (debug or release) and projects to build

4. Build the selected configuration

### Checking issues for the User Interface ###

If you face problems with the user interface and want to check where they come from or how they could be resolved please open the following link in your browser:
http://localhost:55378 **(Wildstar Studio has to be open!)**

It will open a view similar to the chrome developer tools and you can work with the javascript console, modify DOM elements and so on. If you have an UI issue and are able to find a solution using the development tools please attach that info to your issue.