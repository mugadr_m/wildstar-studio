#pragma once

#include <deque>
#include <thread>
#include <future>

class NetThreadPool {
private:

	std::vector<std::thread> mThreads;
	std::deque<std::shared_ptr<std::packaged_task<void()>>> mTasks;
	std::condition_variable mDataSignal;
	std::mutex mDataLock;

	bool mIsRunning = true;

	void join();
public:
	void initialize();
	void shutdown();

	void execute(const std::shared_ptr<std::packaged_task<void()>>& task);

	static std::shared_ptr<NetThreadPool> getInstance() {
		static std::shared_ptr<NetThreadPool> gInstance = std::make_shared<NetThreadPool>();
		return gInstance;
	}
};

#define sNetThreadPool (NetThreadPool::getInstance())