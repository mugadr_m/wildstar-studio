#include "stdafx.h"
#include "HttpRequest.h"
#include "NetThreadPool.h"

size_t _internal_curl_write_callback(void *buffer, size_t size, size_t nmemb, void *userp) {
	uint32 numBytes = size * nmemb;
	HttpRequest* req = (HttpRequest*) userp;

	if (req->mRawCallback) {
		req->mRawCallback((uint8*) buffer, numBytes);
		return numBytes;
	}

	if (req->mIsBinary == false) {
		char* cb = (char*) buffer;
		std::string strResp(cb, cb + numBytes);


		req->mResponse += String::toUnicode(strResp);
	} else {
		req->mBinaryContent.insert(req->mBinaryContent.end(), (uint8*) buffer, ((uint8*) buffer) + numBytes);
	}

	return size * nmemb;
}

int _internal_curl_progress(void *p, double dltotal, double dlnow, double ultotal, double ulnow) {
	HttpRequest* req = (HttpRequest*) p;
	req->onDownloadProgress(dlnow, dltotal);
	return 0;
}

HttpRequest::HttpRequest(const std::wstring& address) {
	mIsBinary = false;

	mCurl = curl_easy_init();
	curl_easy_setopt(mCurl, CURLOPT_FRESH_CONNECT, true);
	curl_easy_setopt(mCurl, CURLOPT_FORBID_REUSE, true);
	curl_easy_setopt(mCurl, CURLOPT_FOLLOWLOCATION, 1L);
	curl_easy_setopt(mCurl, CURLOPT_SSL_VERIFYPEER, 0L);
	curl_easy_setopt(mCurl, CURLOPT_SSL_VERIFYHOST, 0L);
	curl_easy_setopt(mCurl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36");
	curl_easy_setopt(mCurl, CURLOPT_URL, String::toAnsi(address).c_str());
	curl_easy_setopt(mCurl, CURLOPT_WRITEFUNCTION, _internal_curl_write_callback);
	curl_easy_setopt(mCurl, CURLOPT_PROGRESSFUNCTION, _internal_curl_progress);
	curl_easy_setopt(mCurl, CURLOPT_PROGRESSDATA, this);
	curl_easy_setopt(mCurl, CURLOPT_NOPROGRESS, 0L);
	curl_easy_setopt(mCurl, CURLOPT_WRITEDATA, this);
}

void HttpRequest::onDownloadProgress(double now, double total) {
	if (mProgressCallback) {
		mProgressCallback(now, total);
	}
}

std::wstring HttpRequest::getResponseSync() {
	auto res = curl_easy_perform(mCurl);
	if (res != CURLE_OK) {
		throw std::exception("Unable to retrieve data!");
	}

	return mResponse;
}

const std::vector<uint8>& HttpRequest::getResponseSyncBinary(std::function<void(double, double)> progress) {
	mProgressCallback = progress;
	mIsBinary = true;

	auto res = curl_easy_perform(mCurl);
	if (res != CURLE_OK) {
		throw std::exception("Unable to retrieve data!");
	}

	return mBinaryContent;
}

void HttpRequest::asyncGetResponse(std::function<void (std::wstring)> callback, std::function<void (double, double)> progress) {
	auto _this = shared_from_this();
	mProgressCallback = progress;

	sNetThreadPool->execute(std::make_shared<std::packaged_task<void()>>([callback, _this]() {
		callback(_this->getResponseSync());
	}));
}

void HttpRequest::asyncGetResponseBinary(std::function<void (const std::vector<uint8>&)> callback, std::function<void (double, double)> progress) {
	auto _this = shared_from_this();
	mProgressCallback = progress;
	mIsBinary = true;

	sNetThreadPool->execute(std::make_shared<std::packaged_task<void()>>([callback, _this]() {
		auto res = curl_easy_perform(_this->mCurl);
		if (res != CURLE_OK) {
			throw std::exception("Unable to retrieve data!");
		}

		callback(_this->mBinaryContent);

	}));
}