#include "stdafx.h"
#include "NetThreadPool.h"

#include <future>

void NetThreadPool::join() {
	while (mIsRunning) {
		std::shared_ptr<std::packaged_task<void()>> task;
		{
			std::unique_lock<std::mutex> l(mDataLock);
			mDataSignal.wait(l, [this]() { return mIsRunning == false || mTasks.size() > 0;  });
			if (mIsRunning == false) {
				return;
			}

			task = mTasks.front();
			mTasks.pop_front();
		}

		(*task)();
	}
}

void NetThreadPool::initialize() {
	for (int i = 0; i < 4; ++i) {
		mThreads.push_back(std::thread([this]() {
			join();
		}));
	}
}

void NetThreadPool::shutdown() {
	mIsRunning = false;
	mDataSignal.notify_all();
	for (auto& thread : mThreads) {
		if (thread.joinable()) {
			thread.join();
		}
	}
}

void NetThreadPool::execute(const std::shared_ptr<std::packaged_task<void()>>& task) {
	std::unique_lock<std::mutex> l(mDataLock);
	mTasks.push_back(task);
	mDataSignal.notify_one();
}
